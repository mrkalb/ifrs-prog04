package sample;

import javafx.geometry.Pos;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.layout.VBox;
import javafx.stage.Modality;
import javafx.stage.Stage;

public class ConfirmBox {

    // TODO (1) - Criar uma variável estática booleana chamada 'answer'


    // TODO 10 - Mudar o retorno do método para corresponder com o tipo de'answer'
    public static void display(String title, String message) {
        Stage window = new Stage();
        window.initModality(Modality.APPLICATION_MODAL);

        window.setTitle(title);
        window.setMinWidth(250);

        Label label = new Label();
        label.setText(message);

        // TODO (2) - Criar dois botões chamandos 'yesBtn' e 'noBtn'

        // Armazena a resposta e fecha a janela
        yesBtn.setOnAction(e -> {
            // TODO (3) - Setar o valor da variável 'answer'
            // TODO (4) - Fechar a janela
        });
        noBtn.setOnAction(e -> {
            // TODO (5) - Setar o valor da variável 'answer'
            // TODO (6) - Fechar a janela
        });

        VBox layout = new VBox(10);

        // TODO (7) - Adicionar os dois botões ao 'layout'

        layout.setAlignment(Pos.CENTER);

        Scene scene = new Scene(layout);
        window.setScene(scene);
        window.showAndWait();

        // TODO (8) - Retornar 'answer'
    }

}
