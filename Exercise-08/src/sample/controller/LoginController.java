package sample.controller;

import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Button;
import javafx.scene.control.PasswordField;
import javafx.scene.control.TextField;
import sample.Main;

import java.net.URL;
import java.util.ResourceBundle;

public class LoginController implements Initializable {
    Main main;

    // TODO (8) - Ligue o botão login a uma variável
    // TODO (9) - Ligue a caixa de texto do login a uma variável
    // TODO (10) - Ligue a caixa de texto da senha a uma variável

    public void onLogin() {
        // TODO (12) - Chame o método 'main.handleLogin'
    }

    @Override
    public void initialize(URL location, ResourceBundle resources) {
        System.out.println("Initialize LoginController");
    }

    public void setMain(Main main) {
        this.main = main;
    }
}
