package sample.controller;

import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.Node;
import javafx.scene.Parent;
import javafx.scene.control.Label;
import javafx.scene.control.ListCell;
import sample.model.Person;

import java.io.IOException;

public class PersonListCellController extends ListCell<Person> {
    private Node graphic;

    @FXML
    public Label firstName;

    @FXML
    public Label lastName;

    PersonListCellController() {
        FXMLLoader loader  = new FXMLLoader(getClass().getResource("../view/PersonListCellView.fxml"));
        loader.setController(this);

        try {
            graphic = loader.load();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    @Override
    protected void updateItem(Person item, boolean empty) {
        super.updateItem(item, empty);

        if (item == null || empty || graphic == null) {
            setText(null);
            setGraphic(null);
        } else {
            firstName.setText(item.getFirstName());
            lastName.setText(item.getLastName());

            setGraphic(graphic);
        }
    }
}
