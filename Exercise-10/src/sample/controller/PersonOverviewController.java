package sample.controller;

import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import sample.Main;
import sample.model.Person;

import java.net.URL;
import java.util.ResourceBundle;

public class PersonOverviewController implements Initializable {
    Main main;

    @FXML
    private TableView<Person> personTable;

    @FXML
    private TableColumn<Person, String> firstNameColumn;

    @FXML
    private TableColumn<Person, String> lastNameColumn;

    @Override
    public void initialize(URL location, ResourceBundle resources) {
        firstNameColumn.setCellValueFactory(elt -> elt.getValue().firstNameProperty());
        //firstNameColumn.setCellFactory(cell -> new PersonTableCellController());

        lastNameColumn.setCellValueFactory(elt -> elt.getValue().lastNameProperty());
    }

    public void setMain(Main main) {
        this.main = main;
        personTable.setItems(main.getPersonData());
    }
}
