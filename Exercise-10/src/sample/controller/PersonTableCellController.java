package sample.controller;

import javafx.fxml.FXMLLoader;
import javafx.scene.Node;
import javafx.scene.control.TableCell;
import sample.model.Person;

import java.io.IOException;

public class PersonTableCellController extends TableCell<Person, String> {
    private Node graphic;


    public PersonTableCellController() {
        FXMLLoader loader = new FXMLLoader(getClass().getResource("../view/PersonTableCellView.fxml"));
        loader.setController(this);

        try {
            graphic = loader.load();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    @Override
    protected void updateItem(String item, boolean empty) {
        super.updateItem(item, empty);

        if (item == null || empty) {
            setText(null);
            setGraphic(null);
        } else {
            setGraphic(graphic);
        }
    }
}
