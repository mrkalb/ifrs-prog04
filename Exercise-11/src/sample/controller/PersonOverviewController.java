package sample.controller;

import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Label;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import sample.Main;
import sample.model.Person;

import java.net.URL;
import java.util.ResourceBundle;

public class PersonOverviewController implements Initializable {
    Main main;

    @FXML
    private TableView<Person> personTable;

    @FXML
    private TableColumn<Person, String> firstNameColumn;

    @FXML
    private TableColumn<Person, String> lastNameColumn;


    // TODO (1) - Declare uma variável do tipo Label chamada firstName e anote com @FXML

    // TODO (2) - Declare uma variável do tipo Label chamada lastName e anote com @FXML

    @Override
    public void initialize(URL location, ResourceBundle resources) {
        firstNameColumn.setCellValueFactory(elt -> elt.getValue().firstNameProperty());
        //firstNameColumn.setCellFactory(cell -> new PersonTableCellController());

        lastNameColumn.setCellValueFactory(elt -> elt.getValue().lastNameProperty());

        // TODO (7) - Adcione um ChangeListener utilizando o método 'personTable.getSelectionModel().selectedItemProperty().addListener'
        // TODO (8) - Chame showDetail passando newValue
    }

    public void setMain(Main main) {
        this.main = main;
        personTable.setItems(main.getPersonData());
    }

    // TODO (5) - Crie um método chamado showDetail que recebe uma variável do tipo Person
    // TODO (6) - Verifique se person != null, caso positivo atualize os valores de firstName e lastName

    // TODO (9) - Implemente o método handleDelete e anote com @FXML
    // TODO (10) - Descubra o item selecionado com 'personTable.getSelectionModel().getSelectedIndex()', e o remova da personTable
        // TODO (12) - Verifique se o item selecionado é valido, caso negativo avise o usuário com uma AlertBox
}
