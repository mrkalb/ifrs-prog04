package sample;

import javafx.application.Application;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.image.Image;
import javafx.scene.layout.BorderPane;
import javafx.stage.Stage;
import sample.controller.LoginController;
import sample.controller.MainController;
import sample.controller.PersonOverviewController;
import sample.model.Person;
import sample.model.PersonListWrapper;

import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Marshaller;
import javax.xml.bind.Unmarshaller;
import java.io.File;
import java.io.IOException;
import java.util.prefs.Preferences;

public class Main extends Application {

    private final String title = "ADS - Programação IV";

    private Stage primaryStage;
    private BorderPane mainView;
    private ObservableList<Person> personData = FXCollections.observableArrayList();

    @Override
    public void start(Stage primaryStage) throws Exception {
        this.primaryStage = primaryStage;
        primaryStage.setTitle(title);

        // initializes primary layout
        initMainView();

        //setScene(loadLoginView());
        handleLogin("admin", "admin");
    }

    public ObservableList<Person> getPersonData() {
        return personData;
    }

    public void setPersonData(ObservableList<Person> personData) {
        this.personData = personData;
    }

    private void setScene(Parent root) {
        if (primaryStage.getScene() == null) {
            Scene scene = new Scene(root);
            primaryStage.setScene(scene);
        } else {
            primaryStage.getScene().setRoot(root);
        }

        primaryStage.getIcons().add(new Image(Main.class.getResourceAsStream("../res/images/icon.png")));
        primaryStage.sizeToScene();
        primaryStage.show();
    }

    private void initMainView() throws IOException {
        FXMLLoader loader = new FXMLLoader();
        loader.setLocation(getClass().getResource("view/MainView.fxml"));

        mainView = loader.load();

        ((MainController) loader.getController()).setMain(this);
    }

    private Parent loadLoginView() throws IOException {
        FXMLLoader loader = new FXMLLoader();
        loader.setLocation(getClass().getResource("view/LoginView.fxml"));

        Parent root = loader.load();

        ((LoginController) loader.getController()).setMain(this);

        return root;
    }

    private Parent loadPersonOverviewView() throws IOException {
        FXMLLoader loader = new FXMLLoader();
        loader.setLocation(getClass().getResource("view/PersonOverviewView.fxml"));

        Parent root = loader.load();

        ((PersonOverviewController) loader.getController()).setMain(this);

        // TODO(28) - Chame 'getPersonFile()' e obtenha um File
        File file = null;
        if (file != null) {
            // TODO(29) - Chame o método 'loadFromFile(file)'
        }

        return root;
    }

    public boolean handleLogin(String text, String passwordFieldText) {
        if (text.equals("admin") && passwordFieldText.equals("admin")) {
            try {
                mainView.setCenter(loadPersonOverviewView());
                setScene(mainView);
                return true;
            } catch (IOException e) {
                e.printStackTrace();
                return false;
            }
        } else {
            return false;
        }
    }

    public void handleLogout() {
        // clear data
        personData.clear();

        try {
            setScene(loadLoginView());
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public File getPersonFile() {
        Preferences prefs = Preferences.userNodeForPackage(Main.class);
        // TODO(18) - Utilizando 'prefs', chame o método 'prefs.get("file", null)' e obtenha 'path'
        String path = null;

        if (path != null) {
            // TODO(19) - Retorne um novo arquivo utilizando 'new File(path)'
            return null;
        } else {
            return null;
        }
    }

    public void setPersonFile(File file) {
        Preferences prefs = Preferences.userNodeForPackage(Main.class);

        if (file != null) {
            // TODO(20) - Utilizando 'prefs', chame o método 'prefs.put("file", file.getPath()'
            // TODO(21) -  Modifique o título da janela para 'title + " - " + file.getName()'
        } else {
            prefs.remove("file");
            primaryStage.setTitle(title);
        }
    }

    public void loadFromFile(File file) {
        try {
            JAXBContext context = JAXBContext.newInstance(PersonListWrapper.class);
            Unmarshaller um = context.createUnmarshaller();

            // TODO(03) - Chame o método 'um.unmarshal(file)' para obter o PersonListWrapper

            // TODO(04) - Limpe 'personData'e adicione os elementos carregados de wrapper

            // TODO(05) - Modifique o título da janela para 'title + " - " + file.getName()'
        } catch (JAXBException e) {
            e.printStackTrace();
        }
    }

    public void saveToFile(File file) {
        try {
            JAXBContext context = JAXBContext.newInstance(PersonListWrapper.class);
            Marshaller m = context.createMarshaller();
            m.setProperty(Marshaller.JAXB_FORMATTED_OUTPUT, true);

            // TODO(06) - Crie uma instância de 'PersonListWrapper'
            PersonListWrapper wrapper = null;
            // TODO(07) - Utilizando 'personData', chame o método 'setPersons' de wrapper

            m.marshal(wrapper, file);

            // TODO(22) - Chame o método 'setPersonFile(file)'
        } catch (JAXBException e) {
            e.printStackTrace();
        }
    }
}
