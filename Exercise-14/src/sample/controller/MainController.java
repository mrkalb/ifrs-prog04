package sample.controller;

import javafx.application.Platform;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.stage.FileChooser;
import sample.Main;

import java.io.File;
import java.net.URL;
import java.util.ResourceBundle;

public class MainController implements Initializable {
    Main main;

    @Override
    public void initialize(URL location, ResourceBundle resources) {
    }

    public void setMain(Main main) {
        this.main = main;
    }

    @FXML
    private void handleLogout() {
        main.handleLogout();
    }

    @FXML
    private void handleClose() {
        Platform.exit();
    }

    @FXML
    private void handleNew() {
        // TODO(23) - Limpe personData
        // TODO(24) - Chame o método 'main.setPersonFile(null)'
    }

    @FXML
    private void handleOpen() {
        // TODO(12) - Crie uma intância de 'FileChooser'

        // TODO(13) - Chame o método 'fileChooser.showOpenDialog' e obtenha um File
        File file = null;

        if (file != null) {
            // TODO(14) - Chame o método 'main.loadFromFile(file)'
        }
    }

    @FXML
    private void handleSave() {
        // TODO (25) - Obtenha o arquivo atual utilizando 'main.getPersonFile()'
        File file = null;

        if (file != null) {
            // TODO(26) - Chame o método 'main.saveToFile(file)'
        } else {
            // TODO(27) - Caso contrário, chame 'handleSaveAs()'
        }
    }

    @FXML
    private void handleSaveAs() {
        // TODO(15) - Crie uma intância de 'FileChooser'

        // TODO(16) - Chame o método 'fileChooser.showSaveDialog' e obtenha um File
        File file = null;

        if (file != null) {
            // TODO(17) - Chame o método 'main.saveToFile(file)'
        }
    }
}
